import { FC } from "react";
import { useRouter } from "next/router";

type ActiveLinkProps = {
    children: any,
    href: string
}

const ActiveLink: FC<ActiveLinkProps> = ({ children, href }) => {
    const router = useRouter()
    const style = {
        marginRight: 10,
        color: router.asPath === href ? 'red' : 'white',
        textDecoration: "none",
    }

    const handleClick = (e) => {
        e.preventDefault()
        router.push(href)
    }

    return (
        <a href={href} onClick={handleClick} style={style}>
            {children}
        </a>
    )
}
export default ActiveLink;