import { FC } from "react";
import { Card } from "@supabase/ui";
import { definitions } from "../../types/supabase";
import { ActiveLink } from "../ActiveLink";
import { Button } from "@supabase/ui";

type BookProps = {
    book: definitions["books"]
}

const Book: FC<BookProps> = ({book}) => {
    return (
        <Card title={book.name}>
            <p style={{color: "white"}}>
            {`${book.name} - ${book.author} -> ${book.pages} pages `}
            <ActiveLink href={"/books/" + book.id}>
                <Button>Detail</Button>
            </ActiveLink>
            </p>
        </Card>
    )
}

export default Book;
