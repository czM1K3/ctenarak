import { FC } from "react";
import ReactLoading from "react-loading";

const Loading: FC = () => {
    return <ReactLoading type="bars" color="white" height={'50%'} width={'50%'} />
}

export default Loading;
