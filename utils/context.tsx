import { createContext, useState, useContext } from "react";

export const RandomContext = createContext(null);

export const RandomProvider = ({children}) => {
    const [random, setRandom] = useState(false);

    return (
        <RandomContext.Provider value={{random,setRandom}}>
            {children}
        </RandomContext.Provider>
    )
}

export const useRandom = () => {
    const random = useContext(RandomContext);
    return [random.random, random.setRandom];
}
