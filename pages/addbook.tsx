import { FC, useState } from "react";
import {
    Formik,
    Form,
    Field,
} from "formik";
import { supabase } from "../utils/initSupabase";
import { definitions } from "../types/supabase";
import { useRouter } from "next/router";
import { FormValues, validate } from "../types/form";

const AddBook: FC = () => {
    const router = useRouter();
    const [user] = useState(supabase.auth.user())

    const initialValues: FormValues = {
        name: "",
        author: "",
        pages: 0
    }

    const submit = async (values: FormValues) => {
        const { data: book, error} = await supabase.from<definitions["books"]>("books").insert({
            user_id: user.id,
            ...values
        }).single();
        if (error) console.log(error);
        else router.push("/books/" + book.id);
    }

    return (
        <>
            <h1>Add book</h1>
            <Formik
                initialValues={initialValues}
                onSubmit={submit}
                validate={validate}
            >
                {({errors, touched}) => (
                    <Form>
                        <label htmlFor="name">Name</label>
                        <Field name="name" id="name" placeholder="Name" />
                        { errors.name && touched.name ? (
                            <div>{errors.name}</div>
                        ) : null }

                        <label htmlFor="author">Author</label>
                        <Field name="author" id="author" />
                        { errors.author && touched.author ? (
                            <div>{errors.author}</div>
                        ) : null }

                        <label htmlFor="pages">Pages</label>
                        <Field name="pages" id="pages" />
                        { errors.pages && touched.pages ? (
                            <div>{errors.pages}</div>
                        ) : null }

                        <button type="submit">Submit</button>
                    </Form>
                )}                
            </Formik>
        </>
    );
}

export default AddBook;