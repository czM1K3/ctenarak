import { FC, useEffect, useState } from "react";
import { NextPageContext } from "next";
import { supabase } from "../../../utils/initSupabase";
import { definitions } from "../../../types/supabase";
import { ActiveLink } from "../../../components/ActiveLink";
import { useRouter } from "next/router";
import { Button, Typography } from "@supabase/ui";
import { Loading } from "../../../components/Loading";

type BookProps = {
    id: number
}

const Book: FC<BookProps> = ({ id }) => {
    const router = useRouter();
    if (!id) return <h1>This is not a number.</h1>;

    const [book, setBook] = useState<definitions["books"] | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getBook = async () => {
            const { data, error } = await supabase.from<definitions["books"]>("books").select("*").eq("id", id).single();
            if (error) console.log(error);
            else setBook(data);
            setLoading(false);
        }
        getBook();
    }, []);

    const deleteBook = async () => {
        try {
            await supabase.from("books").delete().eq("id", book.id);
            router.push("/books");
        }
        catch (error) {
            console.log(error);
        }
    }

    if (book) return (
        <>
            <Typography.Title level={1}>{book.name}</Typography.Title>
            <Typography.Text>Author: {book.author}</Typography.Text><br />
            <Typography.Text>Number of pages: {book.pages}</Typography.Text><br />
            <ActiveLink href="/books/">
                <Button>Back</Button>
            </ActiveLink>
            <ActiveLink href={`/books/${book.id}/edit`}>
                <Button>Edit</Button>
            </ActiveLink>
            <Button onClick={deleteBook}>Delete</Button>
        </>
    )

    if (loading) return <Loading />
    
    return (
        <h1>Nothing found</h1>
    )
}

export const getServerSideProps = async (context: NextPageContext) => {
  return {
    props: {
        id: parseInt(context.query.id.toString())
    }
  }
}

export default Book;
