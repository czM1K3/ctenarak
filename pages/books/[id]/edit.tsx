import { FC, useState, useEffect } from "react";
import { useRouter } from "next/router";
import { definitions } from "../../../types/supabase";
import { supabase } from "../../../utils/initSupabase";
import { NextPageContext } from "next";
import { Loading } from "../../../components/Loading";
import { Formik, Form, Field } from "formik";
import { FormValues, validate } from "../../../types/form";
import { ActiveLink } from "../../../components/ActiveLink";
import { Button } from "@supabase/ui";

type EditProps = {
    id: number
}

const Edit: FC<EditProps> = ({ id }) => {
    const router = useRouter();
    if (!id) return <h1>This is not a number.</h1>;

    const [book, setBook] = useState<definitions["books"] | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const getBook = async () => {
            const { data, error } = await supabase.from<definitions["books"]>("books").select("*").eq("id", id).single();
            if (error) console.log(error);
            else setBook(data);
            setLoading(false);
        }
        getBook();
    }, []);

    
    if (book) {
        const initialValues: FormValues = {
            name: book.name,
            author: book.author,
            pages: book.pages
        }
    
        const submit = async (values: FormValues) => {
            const { error } = await supabase.from<definitions["books"]>("books").update(values).match({name: book.name});
            if (error) console.log(error);
            else router.push("/books/" + book.id);
        }

        return (
            <>
                <h1>Edit book: {book.name}</h1>
                <Formik
                    initialValues={initialValues}
                    onSubmit={submit}
                    validate={validate}
                >
                    {({ errors, touched }) => (
                        <Form>
                            <label htmlFor="name">Name</label>
                            <Field name="name" id="name" placeholder="Name" />
                            {errors.name && touched.name ? (
                                <div>{errors.name}</div>
                            ) : null}

                            <label htmlFor="author">Author</label>
                            <Field name="author" id="author" />
                            {errors.author && touched.author ? (
                                <div>{errors.author}</div>
                            ) : null}

                            <label htmlFor="pages">Pages</label>
                            <Field name="pages" id="pages" />
                            {errors.pages && touched.pages ? (
                                <div>{errors.pages}</div>
                            ) : null}

                            <button type="submit">Submit</button>
                        </Form>
                    )}
                </Formik>
                <ActiveLink href={`/books/${book.id}`}>
                    <Button>Back</Button>
                </ActiveLink>
            </>
        );
    }

    if (loading) return <Loading />;

    return <div>Nothing found</div>;
};

export const getServerSideProps = async (context: NextPageContext) => {
    return {
        props: {
            id: parseInt(context.query.id.toString())
        }
    }
}

export default Edit;
