import { FC, useEffect, useState } from "react";
import { Button } from "@supabase/ui";
import { supabase } from "../../utils/initSupabase";
import { definitions } from "../../types/supabase";
import { Book } from "../../components/Book";
import { ActiveLink } from "../../components/ActiveLink";
import { Loading } from "../../components/Loading";

const Books: FC = () => {
    const [books, setBooks] = useState<definitions["books"][] | null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchBooks = async () => {
            const { data, error } = await supabase.from<definitions["books"]>("books").select("*");
            if (error) console.log(error);
            else setBooks(data);
            setLoading(false);
        }
        fetchBooks();
    }, []);

    if (loading) return <Loading />
    
    if (books) return (
        <>
            <ActiveLink href="/addbook">
                <Button>Add new book</Button>
            </ActiveLink>
            <ActiveLink href="/">
                <Button>Main page</Button>
            </ActiveLink>
            {books.map(x => (
                <Book book={x} key={x.id} />
            ))}
        </>
    )

    return (
        <h1>Nothing found</h1>
    )
}

export default Books;
