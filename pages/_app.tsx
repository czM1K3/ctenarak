import { AppProps } from "next/app";
import { Auth } from '@supabase/ui';
import { supabase } from '../utils/initSupabase';
import './../style.css';
import { RandomProvider } from "../utils/context";

export default function MyApp({ Component, pageProps }: AppProps) {
  return (
    <main className={'dark'}>
      <Auth.UserContextProvider supabaseClient={supabase}>
        <RandomProvider>
          <Component {...pageProps} />
        </RandomProvider>
      </Auth.UserContextProvider>
    </main>
  )
}
