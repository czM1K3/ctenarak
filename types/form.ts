import { FormikErrors } from "formik";

export type FormValues = {
    name: string,
    author: string,
    pages: number
}

export const validate = (values: FormValues) => {
    const errors: FormikErrors<FormValues> = {};
    if (!values.name) errors.name = "Required";
    if (!values.author) errors.author = "Required";
    if (!values.pages) errors.pages = "Required";
    return errors;
}